import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
/**
 * Pantallas
 */
import Account from "../../screens/Account/Account";
import Login from "../../screens/Account/login/Login";
import Register from "../../screens/Account/login/Register";


/**
 * Navegador Stack
 */
const Stack = createStackNavigator();

/**
 * Componente defaul
 */
export default function AccountStack(){

    return (
            <Stack.Navigator>
                {/* Cuenta */}
               <Stack.Screen 
               name="account" 
               component={Account}
               options={{ title: "Cuenta"}}/>
               {/* Login */}
               <Stack.Screen 
               name="login" 
               component={Login}
               options={{ title: "iniciar Sesión"}}/>
               {/* Registro */}

               <Stack.Screen 
               name="register" 
               component={Register}
               options={{ title: "Registro"}}/>

            </Stack.Navigator>
    );

}