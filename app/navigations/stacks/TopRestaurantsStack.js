import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
/**
 * Pantallas
 */
import TopRestaurants from "../../screens/TopRestaurants";



const Stack = createStackNavigator();

export default function TopRestaurantsStack(){

    return (
            <Stack.Navigator>
                {/* Buscar */}
               <Stack.Screen 
               name="top-restaurants" 
               component={TopRestaurants}
               options={{ title: "Los mejores restaurantes"}}/>
              

            </Stack.Navigator>
    );

}