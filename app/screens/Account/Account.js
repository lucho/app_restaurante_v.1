import React, { useState, useEffect } from "react";
import * as firebase from "firebase";
import UserGuest from "./UserGuest";
import UserLogged from "./UserLogged";
import Loading from "../../components/Loading/Loading";





export default function Account() {
  /**
   * Validar estado de inicio de sesión
   */
  const [login, setLogin] = useState(null);

  /**
   * Se dispara al renderizar el componente
   */
  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
        console.log(user);
      !user ? setLogin(false) : setLogin(true);
    });
  }, []);


  if(login === null) return <Loading isVisible={true} text="Cargando..."/>

  return login ? <UserLogged /> : <UserGuest />
}