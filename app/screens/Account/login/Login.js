/**
 * Dependencias
 */
import React from "react";
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import { Divider } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";




/**
 * 
 * Componente defaul
 */
export default function Login() {
  
  return (
    <ScrollView>
      <Image
        source={require("../../../../assets/img/logo/logoNegroNet.png")}
        resizeMode="contain"
        style={styles.logo}
      />

      <View style={styles.viewContainer}>
        <Text> Login FORM</Text>
        <CreateAvvount/>
      </View>
      <Divider style={styles.divider}/>
      <Text>Social Login</Text>
    </ScrollView>
  );
}

/**
 * Componentes adiconales
 */
function CreateAvvount(){
/**
 * 
 * Cnstante para la navegación
 */
 const navigation = useNavigation();
 
  return (
    <Text style={styles.textRegister}>
      ¿Aún no tienes una cuenta ? {" "}
      <Text  
      style={styles.btnRegister}
      onPress={()=> navigation.navigate("register")}
      
      >
        Regístrate
      </Text>
    </Text>
  );
}

/**
 * Estilos
 */
const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 100,
    marginTop: 20,
  },
  viewContainer:{
    marginRight: 40,
    marginLeft:40
  },
  textRegister:{
    marginTop: 15,
    marginLeft: 10,
    marginRight:10
  },
  btnRegister:{
    color: "#00a680", // Color corporativo
    fontWeight: "bold"
  },
  divider:{
    backgroundColor:"#00a680", // Color corporativo
    margin:40
  }
});
