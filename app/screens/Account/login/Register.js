/**
 * Dependencias
 */
import React,{useRef} from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from "react-native-easy-toast";





/**
 * 
 */
import RegisterForm from "../../../components/Account/RegisterForm";
/**
 *
 * Componeente Defaul
 */

export default function Register() {
    /**
     * 
     */
    const toastRef = useRef();
    /**
     * 
     */
  return (
    <KeyboardAwareScrollView>
      <Image
        source={require("../../../../assets/img/logo/logoNegroNet.png")}
        resizeMode="contain"
        style={styles.logo}
      />
      <View style={styles.viewForm}>
          <RegisterForm toastRef={toastRef}/>
      </View>
      {/* Mensaje de error */}
      <Toast ref={toastRef}  position="center" opacity={0.9}/>
    </KeyboardAwareScrollView>
  );
}

/**
 * Estilos
 */

const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 100,
    marginTop: 20,
  },
  viewForm:{
      marginRight:40,
      marginLeft: 40


  }
});
